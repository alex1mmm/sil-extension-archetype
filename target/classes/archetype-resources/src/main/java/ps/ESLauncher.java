#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.ps;


import com.atlassian.event.api.EventPublisher;

import com.atlassian.plugin.spring.scanner.annotation.component.ClasspathComponent;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.lifecycle.LifecycleAware;
import ${package}.routine.SayHello;
import ${package}.routine.SayHello2;
import ${package}.routine.SayHello3;
import ${package}.service.BeanService;
import com.keplerrominfo.refapp.config.CommonPluginConfigurationService;
import com.keplerrominfo.refapp.config.HostConfigurationProvider;
import com.keplerrominfo.refapp.config.PluginInfoService;
import com.keplerrominfo.refapp.config.impl.PluginConfigurationServiceImpl;
import com.keplerrominfo.refapp.launcher.AbstractPluginLauncher;
import com.keplerrominfo.sil.lang.RoutineRegistry;
import lombok.extern.slf4j.Slf4j;



import javax.inject.Inject;
import javax.inject.Named;

@ExportAsService(LifecycleAware.class)
@Named
@Slf4j
public class ESLauncher extends AbstractPluginLauncher implements LifecycleAware {

    private final BeanService beanService;

    @Inject
    public ESLauncher(@ComponentImport EventPublisher eventPublisher,
                      PluginInfoService pluginInfoService,
                      @ComponentImport CommonPluginConfigurationService commonPluginConfigurationService,
                      @ComponentImport HostConfigurationProvider hostConfigurationProvider,
                      @ClasspathComponent PluginConfigurationServiceImpl pluginConfigurationService,
                      BeanService beanService)
    {
        super(eventPublisher, pluginInfoService, hostConfigurationProvider, pluginConfigurationService);

        log.error("eslauncher constructor");
        this.beanService = beanService;

    }

    @Override
    public void doAtLaunch() {
        super.doAtLaunch();
        log.error("eslauncher doatlaunch");
        RoutineRegistry.register(new SayHello( beanService,"SayHello"));
        RoutineRegistry.register(new SayHello2( beanService,"SayHello2"));
        RoutineRegistry.register(new SayHello3( beanService,"SayHello3"));

    }

    @Override
    public void doAtStop() {
        log.error("eslauncher doatstop");

        try {
            RoutineRegistry.unregister("SayHello");
            RoutineRegistry.unregister("SayHello2");
            RoutineRegistry.unregister("SayHello3");
        } catch(Exception e) {
            log.error("Failed to unregister!", e);
        }
        super.doAtStop();
    }

    @Override
    public void afterPropertiesSet() {
        super.afterPropertiesSet();
        this.onStart();
    }
}

