#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.routine;


import com.keplerrominfo.sil.lang.*;

import lombok.extern.slf4j.Slf4j;
import java.util.List;

@Slf4j
public abstract class AbstractSILRoutine<T> extends AbstractRoutine<T> {
    private final ClassLoader classLoader;

    public AbstractSILRoutine (ClassLoader classLoader,String name, SILType[][] signatures) {
        super(name, signatures);
        this.classLoader = classLoader;
    }

    @Override
    protected SILValue<T> executeRoutine(SILContext silContext, List<SILValue<?>> list) {
        log.debug("executeRoutine in");
        ClassLoader original = Thread.currentThread().getContextClassLoader();

        try {
            Thread.currentThread().setContextClassLoader(this.classLoader);


            return this.runRoutine(silContext, list);
        }
        finally {
            Thread.currentThread().setContextClassLoader(original);
        }

    }

    protected abstract SILValue<T> runRoutine(SILContext silContext, List<SILValue<?>> list) ;

}

