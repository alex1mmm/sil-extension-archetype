#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.routine;

import com.keplerrominfo.common.util.MutableString;
import com.keplerrominfo.sil.lang.*;
import com.keplerrominfo.sil.lang.type.TypeInst;
import lombok.extern.slf4j.Slf4j;
import ${package}.service.BeanService;

import java.util.ArrayList;
import java.util.List;


@Slf4j
public class SayHello2 extends AbstractSILRoutine<KeyableArraySILObject<MutableString>> {
    private static final SILType[][] types = {{TypeInst.STRING, TypeInst.STRING}};
    private final BeanService beanService;

    public SayHello2(BeanService beanService, String name) {
        super(beanService.getClassLoaderService().getPluginClassLoader(), name, types);
        this.beanService = beanService;

    }

    @Override
    public SILType<KeyableArraySILObject<MutableString>> getReturnType() {
        return TypeInst.STRING_ARR;
    }


    @Override
    protected SILValue<KeyableArraySILObject<MutableString>> runRoutine(SILContext silContext, List<SILValue<?>> list) {
        SILValue param1 = list.get(0);
        SILValue param2 = list.get(1);
        List<String> res = new ArrayList<>();
        res.add("Hello " + param1.toStringValue());
        res.add("Hello " + param2.toStringValue());
        return SILValueFactory.stringArray(res);

    }

    @Override
    public String getParams() {
        return "(name1, name2)";
    }
}
