#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.routine;

import com.keplerrominfo.common.util.MutableString;
import com.keplerrominfo.sil.lang.*;
import com.keplerrominfo.sil.lang.type.TypeInst;
import lombok.extern.slf4j.Slf4j;
import ${package}.service.BeanService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Slf4j
public class SayHello3 extends AbstractSILRoutine<KeyableArraySILObject<MutableString>> {
    private static final SILType[][] types = {{TypeInst.STRING, TypeInst.STRING}};
    private final BeanService beanService;

    public SayHello3(BeanService beanService, String name) {
        super(beanService.getClassLoaderService().getPluginClassLoader(), name, types);
        this.beanService = beanService;
    }

    @Override
    public SILType<KeyableArraySILObject<MutableString>> getReturnType() {
        return TypeInst.STRING_ARR;
    }


    @Override
    protected SILValue<KeyableArraySILObject<MutableString>> runRoutine(SILContext silContext, List<SILValue<?>> list) {
        SILValue param1 = list.get(0);
        SILValue param2 = list.get(1);
        Map<String, String> res = new HashMap<>();
        res.put("param1","Hello " + param1.toStringValue());
        res.put("param2","Hello " + param2.toStringValue());
        return SILValueFactory.stringArray(res);

    }

    @Override
    public String getParams() {
        return "(name1, name2)";
    }
}

