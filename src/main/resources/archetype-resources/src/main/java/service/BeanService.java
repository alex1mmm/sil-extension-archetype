#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.service;


import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.keplerrominfo.jira.commons.hostapp.JMIssueSearchServices;
import com.keplerrominfo.jira.commons.hostapp.JMIssueServices;
import com.keplerrominfo.jira.commons.hostapp.KIssueService;
import com.keplerrominfo.jira.commons.user.CurrentUserHelper;
import com.keplerrominfo.jira.commons.user.UserHelper;
import lombok.Getter;

import javax.inject.Inject;
import javax.inject.Named;

@Named
public class BeanService {
    @Getter
    private final KIssueService kIssueService;
    @Getter
    private final ClassLoaderService classLoaderService;
    @Getter
    private final CurrentUserHelper currentUserHelper;
    @Getter
    private final JMIssueSearchServices jmIssueSearchServices;
    @Getter
    private final UserHelper userHelper;
    @Getter
    private final JMIssueServices jmIssueServices;
    @Inject
    public BeanService(@ComponentImport KIssueService kIssueService,
                       @ComponentImport CurrentUserHelper currentUserHelper,
                       @ComponentImport JMIssueSearchServices jmIssueSearchServices,
                       @ComponentImport UserHelper userHelper,
                       @ComponentImport JMIssueServices jmIssueServices,
                       ClassLoaderService classLoaderService) {
        this.kIssueService = kIssueService;
        this.classLoaderService = classLoaderService;
        this.currentUserHelper = currentUserHelper;
        this.jmIssueSearchServices = jmIssueSearchServices;
        this.userHelper = userHelper;
        this.jmIssueServices = jmIssueServices;
    }
}
