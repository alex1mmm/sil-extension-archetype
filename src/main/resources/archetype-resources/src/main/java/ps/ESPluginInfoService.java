#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.ps;


import ${package}.service.ResourceService;
import com.keplerrominfo.refapp.config.PluginInfoService;

import javax.annotation.Nonnull;
import javax.inject.Inject;
import javax.inject.Named;

@Named
public class ESPluginInfoService implements PluginInfoService {
    @Inject
    ResourceService resourceService;

    @Nonnull
    @Override
    public String getKey() {
        return resourceService.getProperty("atlassian.plugin.key");
    }

    @Nonnull
    @Override
    public String getName() {
        return resourceService.getProperty("atlassian.plugin.key");
    }
}

